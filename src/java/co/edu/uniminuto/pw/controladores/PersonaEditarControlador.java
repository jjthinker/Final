/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.uniminuto.pw.controladores;

import co.edu.uniminuto.pw.DAOs.PersonaDAO;
import co.edu.uniminuto.pw.DTOs.Persona;
import co.edu.uniminuto.pw.bds.MySqlDataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author docentes
 */
public class PersonaEditarControlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            PersonaDAO pDao = new PersonaDAO();
            
            Logger.getLogger(PersonaEditarControlador.class.getName()).log(Level.INFO, "Ejecutando PersonaEditarControlador...");
            
            
            int id = Integer.parseInt(request.getParameter("id"));
            String ident = request.getParameter("identificacion");
            String nombre1 = request.getParameter("nombre1");
            String nombre2 = request.getParameter("nombre2");
            String apellido1 = request.getParameter("apellido1");
            String apellido2 = request.getParameter("apellido2");
            String genero = request.getParameter("genero");
            String tipoP = request.getParameter("tipop");
            String fNacimiento = request.getParameter("fecha");
            String telef = request.getParameter("telefono");
            String email = request.getParameter("email");            
            
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO,"ident="+ident);
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO,"nombre1="+nombre1);
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO,"fNacimiento="+fNacimiento);
            
            
            Persona p = new Persona();
            p.setId(id);
            p.setIdentificacion(ident);
            p.setNombre1(nombre1);
            p.setNombre2(nombre2);
            p.setApellido1(apellido1);
            p.setApellido2(apellido2);
            p.setGenero(genero);
            p.setTipoP(tipoP);
            p.setfNacimiento(fNacimiento);
            p.setTelef(telef);
            p.setEmail(email);                                    
            
            boolean res = pDao.editarPersona(p, MySqlDataSource.getConexionBD());
            
            Logger.getLogger(MySqlDataSource.class.getName()).log(Level.SEVERE, "UPDATE + " + ident + "-" + res);
                        
            if (res)
                request.setAttribute("mensaje", "Se edito satisfactoriamente!!!");
            else
                request.setAttribute("mensaje", "NO se guardaron los cambios...");
                
            RequestDispatcher req = request.getRequestDispatcher("/jsp/persona_editar.jsp");
            req.forward(request, response);
            
        }
        catch (Exception e)
        {
            Logger.getLogger(MySqlDataSource.class.getName()).log(Level.SEVERE, null, e);
        }        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
