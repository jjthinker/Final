
package Controladora;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Juan
 */
public class Conexión {
    private String username="root";
    private String password="12345678";
    private String host="localhost";
    private String port="3306";
    private String database ="loginjsp";
    private String classname="com.mysql.jdbc.Driver";
    private String url="jdbc:mysql://"+host+":"+port+"/"+database;
    private Connection con;
    
    public Conexión(){
        try{
            Class.forName(classname);
            con = DriverManager.getConnection(url,username,password);     
        }catch (ClassNotFoundException e){
            System.out.println("ERROR "+e);
        }catch (SQLException e){
            System.out.println("ERROR "+e);
        }
    }
    public Connection getConexión(){
        return  con;
    }
    public static void main(String[] args) {
        Conexión con = new Conexión();
    }
}
