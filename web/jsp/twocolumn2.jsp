<%-- 
    Document   : twocolumn2
    Created on : 05-oct-2016, 20:04:16
    Author     : Juan
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="co.edu.uniminuto.pw.DTOs.Persona"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

    <head>
        <title>CONTROL DE ACCESO</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'>
        <!-- Codigo JS-->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="../js/skel.min.js" type="text/javascript"></script>
        <script src="../js/skel-panels.min.js" type="text/javascript"></script>
        <script src="../js/init.js" type="text/javascript"></script>
        <script src="../js/main.js" type="text/javascript"></script>
        <script src="../js/html5shiv.js" type="text/javascript"></script>
        <!--Codigo css3-->
        <link href="../css/style-desktop.css" rel="stylesheet" type="text/css"/>
        <link href="../css/skel-noscript.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <link href="../css/ie/v8.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style-1000px.css" rel="stylesheet" type="text/css"/>
        <link href="../css/Style_re.css" rel="stylesheet" type="text/css"/>
    </head>
    
        <div id="header">
            <div class="container">				
                <div id="logo">
                    <h1><a href="#">EDITOR DE USUARIOS EXISTENTES</a></h1>
                </div>
                <nav id="nav">
                    <ul>
                        <li><a href="index.jsp">PAGINA DE INICIO</a></li>
                        <li><a href="threecolumn.jsp">REGISTRAR USUARIO</a></li>
                        <li><a href="twocolumn1.jsp">CONSULTAR</a></li>
                        <li class="active"><a href="twocolumn2.jsp">EDITAR USUARIO</a></li>
                        <li><a href="onecolumn.jsp">GENERAR REPORTES</a></li>
                    </ul>
                </nav>
            </div>
        </div>
       
    <div align='center' id="main">
        <div id="printableArea">
            <table border="0" width="600" style="font-family: Verdana; font-size: 8pt" id="table1"> 
                <tr> 
                    <td colspan="2"><h3 align="center">GENERAR REPORTES</h3></td> 
                </tr>
            </table>

                <div class="signin-form profile">
                    <%
                        ArrayList<Persona> lista = (ArrayList<Persona>) request.getAttribute("lista");
                    %>

                    <div class="login-form">
                        <h1>La Persona</h1>
                        <table>
                            <tr>                                        
                                <th>id</th>
                                <th>Identficacion</th>
                                <th>Nombre</th>
                                <th>Nombre 2</th>
                                <th>Apellido </th>
                                <th>Apellido 1</th>
                                <th>Genero</th>
                                <th>Telefono</th>
                                <th>Email</th>
                                <th>Fecha</th>
                                <th>Tipo personas</th>
                            </tr>
                            <tr>
                                <td><%=lista.get(0).getId()%></td>
                                <td><%=lista.get(0).getIdentificacion()%></td>
                                <td><%=lista.get(0).getNombre1()%></td>
                                <td><%=lista.get(0).getNombre2()%></td>
                                <td><%=lista.get(0).getApellido1()%></td>
                                <td><%=lista.get(0).getApellido2()%></td>
                                <td><%=lista.get(0).getGenero()%></td>
                                <td><%=lista.get(0).getTelef()%></td>
                                <td><%=lista.get(0).getEmail()%></td>
                                <td><%=lista.get(0).getfNacimiento()%></td>
                                <td><%=lista.get(0).getTipoP()%></td>
                            </tr>                             
                        </table> 
                    </div>
                </div>
        </div>
    </div>             

        <div id="Uniminuto">
            <div class="container">
                <a>UNIMINUTO - PROGRAMACION WEB</a> 
            </div>
        </div>

    </body>
</html>
    



