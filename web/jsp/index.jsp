<%-- 
    Document   : index
    Created on : 05-oct-2016, 19:47:18
    Author     : Juan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%--
HttpSession objHttpSession=request.getSession(true);
String usuario= (String)objHttpSession.getAttribute("usuario");
if(usuario.equals("")){
    response.sendRedirect("login.jsp");
}
--%>

<!DOCTYPE html>
<html>
    <head>

        <title>CONTROL DE ACCESO</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'>
        <!-- Codigo JS-->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="../js/skel.min.js" type="text/javascript"></script>
        <script src="../js/skel-panels.min.js" type="text/javascript"></script>
        <script src="../js/init.js" type="text/javascript"></script>
        <script src="../js/main.js" type="text/javascript"></script>
        <script src="../js/html5shiv.js" type="text/javascript"></script>
        <!--Codigo css3-->
        <link href="../css/style-desktop.css" rel="stylesheet" type="text/css"/>
        <link href="../css/skel-noscript.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <link href="../css/ie/v8.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style-1000px.css" rel="stylesheet" type="text/css"/>

    </head>
    <body class="Pagina de inicio">

        <div id="header">
            <div class="container">

                <div id="logo">
                    <h1><a href="#">ADMINISTRACION DE USUARIOS</a></h1>

                </div>
                <nav id="nav">
                    <ul>
                        <li class="active"><a href="index.jsp">Pagina de inicio</a></li>
                        <li><a href="threecolumn.jsp">Registar Usuario</a></li>
                        <li><a href="twocolumn1.jsp">Consultar</a></li>
                        <li><a href="twocolumn2.jsp">Editar Usuarios</a></li>
                        <li><a href="onecolumn.jsp">Generar reporte</a></li>
                    </ul>
                </nav>

            </div>
        </div>
        <div id="main">
            <div class="container">
                <header>
                    <h2>Informacion de interes</h2>
                </header>
                <div class="row">
                    <div class="3u">
                        <section>
                            <hr> <a href="#" class="image full"><img src="../images/pics01.jpg" alt=""/></a>
                            <hr><p>Te permiten controlar los horarios de los empleados de una forma mas eficiente. Puedes calcular la nomina de los empleados con respecto a los horarios de trabajo</p>

                        </section>
                    </div>
                    <div class="3u">
                        <section>
                            <hr><a href="#" class="image full"><img src="../images/pics11.jpg" alt="" /></a>
                            <hr><p>Fomentan una mayor seguridad y control de las visitas a la empresa, si la empresa es grande es fundamental tener un control de accesos que permita mejorar la seguridad de la misma.</p>

                        </section>
                    </div>
                    <div class="3u">
                        <section>
                            <hr><a href="#" class="image full"><img src="../images/pics12.jpg" alt="" /></a>
                            <hr><p>Controlar los accesos y la asistencia permite ahorrar costes en personal, puesto que la productividad empresarial se monitoriza de forma mas adecuada.</p>

                        </section>
                    </div>
                    <div class="3u">
                        <section>
                            <hr><a href="#" class="image full"><img src="../images/pics13.jpg" alt="" /></a>
                            <hr><p>Los sistemas de control de asistencia son perfectos para monitorizar las horas extras para despues pagarlas a los empleados.</p>

                        </section>
                    </div>
                </div>
                <div class="divider">&nbsp;</div>
                <div class="row"></div>		
            </div>
        </div>
        <div id="Uniminuto">
            <div class="container">
                <a>UNIMINUTO - PROGRAMACION WEB</a> 
            </div>
        </div>

    </body>
</html>
