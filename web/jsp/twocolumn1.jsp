<%-- 
    Document   : twocolumn1
    Created on : 05-oct-2016, 20:03:34
    Author     : Juan
--%>

<%@page import="co.edu.uniminuto.pw.DTOs.Persona"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>CONTROL DE ACCESO</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'>
        <!-- Codigo JS-->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="../js/skel.min.js" type="text/javascript"></script>
        <script src="../js/skel-panels.min.js" type="text/javascript"></script>
        <script src="../js/init.js" type="text/javascript"></script>
        <script src="../js/main.js" type="text/javascript"></script>
        <script src="../js/html5shiv.js" type="text/javascript"></script>
        <!--Codigo css3-->
        <link href="../css/style-desktop.css" rel="stylesheet" type="text/css"/>
        <link href="../css/skel-noscript.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <link href="../css/ie/v8.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style-1000px.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="header">
            <div class="container">
                <div id="logo">
                    <h1><a href="#">CONSULTAR REGISTRO DE USUARIOS</a></h1>
                </div>
                <nav id="nav">
                    <ul>
                        <li><a href="index.jsp">PAGINA DE INICIO</a></li>
                        <li><a href="threecolumn.jsp">REGISTRAR USUARIO</a></li>
                        <li class="active"><a href="twocolumn1.jsp">CONSULTAR</a></li>
                        <li><a href="twocolumn2.jsp">EDITAR USUARIOS</a></li>
                        <li><a href="onecolumn.jsp">GENERAR REPORTES</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div id="main">
            <div align="center"> 
                <div class="container">
                    <div class="signin-form profile">
                        <tr> 
                            <td colspan="2"><h3 align="center">EDITAR USUARIOS</h3></td> 
                        </tr> 

                        <div class="login-form">
                            <form action="./PersonaEditarConsultaControlador" method="post">
                                <input type="text" name="identificacion" placeholder="Identificación" required>
                                <input type="text" name="nombre1" placeholder="Nombre 1" required>

                                <input type="submit" value="CONSULTAR">
                            </form>
                        </div>				
                    </div>   
                    <%
                        Persona p = (Persona) request.getAttribute("persona");
                        String msg = (String) request.getAttribute("mensaje");
                        if (msg == null) {
                            msg = "";
                        }
                    %>
                    <div class="signin-form profile">
                        <h3>:: Editar ::</h3>
                        <p><span><%=msg%></span></p>
                        <div class="login-form">
                            <form action="./PersonaEditarControlador" method="post">
                                <input type="text" name="identificacion" value="<%=p == null ? "" : p.getIdentificacion()%>" >
                                <input type="text" name="nombre1" value="<%=p == null ? "" : p.getNombre1()%>" >
                                <input type="text" name="nombre2" value="<%=p == null ? "" : p.getNombre2()%>" >
                                <input type="text" name="apellido1" value="<%=p == null ? "" : p.getApellido1()%>" >
                                <input type="text" name="apellido2" value="<%=p == null ? "" : p.getApellido2()%>" >
                                <input type="text" name="genero" value="<%=p == null ? "" : p.getGenero()%>" >
                                <input type="text" name="tipop" value="<%=p == null ? "" : p.getTipoP()%>" >
                                <input type="date" name="fecha" value="<%=p == null ? "" : p.getfNacimiento()%>" >
                                <input type="text" name="telefono" value="<%=p == null ? "" : p.getTelef()%>" >
                                <input type="email" name="email" value="<%=p == null ? "" : p.getEmail()%>" >

                                <input type="hidden" name="id" value="<%=p == null ? "" : p.getId()%>">

                                <input type="submit" value="EDITAR">
                            </form>
                        </div>
                        <p>${mensaje}</p>
                    </div>
                </div>
            </div> 
        </div>
        <div id="Uniminuto">
            <div class="container">
                <a>UNIMINUTO - PROGRAMACION WEB</a> 
            </div>
        </div>

    </body>
</html>
