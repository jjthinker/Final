<%-- 
    Document   : threecolumn
    Created on : 05-oct-2016, 20:02:33
    Author     : Juan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>CONTROL DE ACCESO</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'>
        <!-- Codigo JS-->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="../js/skel.min.js" type="text/javascript"></script>
        <script src="../js/skel-panels.min.js" type="text/javascript"></script>
        <script src="../js/init.js" type="text/javascript"></script>
        <script src="../js/main.js" type="text/javascript"></script>
        <script src="../js/html5shiv.js" type="text/javascript"></script>
        <!--Codigo css3-->
        <link href="../css/style-desktop.css" rel="stylesheet" type="text/css"/>
        <link href="../css/skel-noscript.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <link href="../css/ie/v8.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style-1000px.css" rel="stylesheet" type="text/css"/>
        <link href="../css/login_Style.css" rel="stylesheet" type="text/css"/>
        <link href="../css/Style_re.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="header">
            <div class="container">
                <div id="logo">
                    <h1><a href="#">REGISTRO DE USUARIOS NUEVOS</a></h1>
                </div>
                <nav id="nav">
                    <ul>
                        <li><a href="index.jsp">PAGINA DE INICIO</a></li>
                        <li class="active"><a href="threecolumn.jsp">REGISTRAR USUARIOS</a></li>
                        <li><a href="twocolumn1.jsp">CONSULTAR</a></li>
                        <li><a href="twocolumn2.jsp">EDITAR USUARIOS</a></li>
                        <li><a href="onecolumn.jsp">GENERAR REPORTES</a></li>
                    </ul>
                </nav>

            </div>
        </div>
        <div align='center' id="main">
            <h3>REGISTRAR USUARIO</h3>
            <div class="login-form">
                <form action="../controladorPersonaCrear" method="post">
                    <input type="text" name="identificacion" placeholder="Identificación" required>
                    <input type="text" name="nombre1" placeholder="Nombre 1" required>
                    <input type="text" name="nombre2" placeholder="Nombre 2" required>
                    <input type="text" name="apellido1" placeholder="Apellido 1" required>
                    <input type="text" name="apellido2" placeholder="Apellido 2" required>
                    <input type="text" name="genero" placeholder="Genero" required="">
                    <input type="text" name="tipop" placeholder="Tipo Persona" required="">
                    <input type="date" name="fecha" placeholder="Fecha Nacimiento" required="">
                    <input type="text" name="telefono" placeholder="Telefono" required="">
                    <input type="email" name="email" placeholder="E-mail" required="">

                    <input type="submit" value="REGISTER">
                </form>
            </div>
            <div class="container">
                <div class="signin-form profile">
                    <h3>:: Registro ::</h3>
                    <%
                        String msg = (String) request.getAttribute("msg");
                    %>
                    <div class="login-form">
                        <h1>La Persona <%=msg%> </h1>
                    </div>
                </div>
            </div>
        </div>
        <div id="Uniminuto">
            <div class="container">
                <a>UNIMINUTO - PROGRAMACION WEB</a> 
            </div>
        </div>

    </body>
</html>

