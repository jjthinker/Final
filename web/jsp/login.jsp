<%-- 
    Document   : login
    Created on : 04-oct-2016, 19:33:40
    Author     : Juan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../css/login_Style.css" rel="stylesheet" type="text/css"/>
        <script src="../js/main.js" type="text/javascript"></script>
    </head>
    <body>
        <div class='login'>
            <h2>Inicio de sesion</h2>
            <form action="Iniciar" method="post" id="forminicio">
                <input name ='username' placeholder='Nombre de usuario' type='text' id="txtusuario"/>
                <input id='txtpass' name='pass' placeholder='Contraseña'  type='password' id="txtpass"/>
                <input type="button" value="Iniciar Sesion" id="btiniciar"/>
            </form>
        </div>      
    </body>
</html>
